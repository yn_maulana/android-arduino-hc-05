//Created By Yusep Maulana - @2019

//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h>
#include <L298N.h>


LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27for a 16 chars and 2 line display
SoftwareSerial module_bluetooth(0, 1); // pin RX | TX
L298N motor(9, 8, 7); // set enable 9, in_1 = 8, in_2 = 7

//configuration for the LCD Module
//GND to GND
//VCC to 5V
//SDA to A4
//SCL to A5

//Configuration for Bluetooth Module
//State not plugged
//RXD to PWM 1
//TXD to PWM 0
//GND to GND
//VCC to 5V
//IN not plugged

//Configuration for L298n
// +5v to +5v
//GND to GND
//IN1 to 8
//IN2 to 7


char data = 0;
int count = 0;
String text;
void setup()
{
  lcd.init();                      // initialize the lcd
  lcd.init();
  lcd.backlight();
  lcd.setCursor(3,0);
  lcd.print("Hello, world!");
  lcd.setCursor(0,1);
  lcd.print("Yusep");

  motor.setSpeed(80); // an integer between 0 and 255

  Serial.begin(9600);
  pinMode(13, OUTPUT);  //inisialisasi LED menjadi output
  delay(5000);
}


void loop()
{
   if(Serial.available() > 0)
  {
    data = Serial.read();    //Pembacaan dan ditampilkan data yang masuk
    //Data yang masuk
    if(data == '1'){
    text = "Mundur";
    motor.stop();
    }
    else if(data == '2'){
     text = "Maju";
      motor.forward();
    }
    else if(data == '3'){
     text = "Kiri";
    }
    else if(data == '4'){
     text = "Kanan";
    }
  }
  lcd.setCursor(0,1);
  lcd.print("Data :");

  lcd.setCursor(8,1);
   lcd.print(text);
  lcd.print("             ");

 Serial.print(text);
}